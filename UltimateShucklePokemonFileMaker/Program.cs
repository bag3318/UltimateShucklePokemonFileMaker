﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

/*
 * |=====================================|
 * | Ultimate Shuckle Pokemon File Maker |
 * |-------------------------------------|
 * | By: bag33188                        |
 * |=====================================|
 */

namespace UltimateShucklePokemonFileMaker
{
    /// <summary>
    /// Represents the program class for the console app.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Represents the main entry point for the console app.
        /// </summary>
        /// <param name="args">A string array of arguments used for CL.</param>
        private static void Main(string[] args)
        {
            // define character for accented 'e' in the word Poke`mon
            char eacute = '\xE9';
            // string hex = Convert.ToByte('é').ToString("x2"); // x

            // Make title information
            Console.BackgroundColor = ConsoleColor.Blue; // make background-of-text color blue
            Console.ForegroundColor = ConsoleColor.Yellow; // make foreground-text color yellow
            StringBuilder titleInfo = new StringBuilder();
            titleInfo.AppendLine("|=====================================|");
            titleInfo.AppendFormat("| Ultimate Shuckle Pok{0}mon File Maker |\r\n", eacute.ToString());
            titleInfo.AppendLine("|-------------------------------------|");
            titleInfo.AppendLine("| By: bag33188                        |");
            titleInfo.AppendLine("|=====================================|");
            Console.WriteLine(titleInfo.ToString() + "\r\n");
            Console.ResetColor();

            // create empty string var for file extension
            string extension = String.Empty;

            // use while loop for case of looping when invalid choice is entered
            while (true)
            {
                // prompt the user and ask what file type they want
                Console.Write("Would you like a *.pk7, *.ek7, *.bin, or *.png file? [1 = pk7, 2 = ek7, 3 = bin, 4 = png] ");
                string askForFileType = Console.ReadLine().Trim();
                if (askForFileType == (1).ToString())
                {
                    extension = ".pk7";
                    break;
                }
                else if (askForFileType == (2).ToString())
                {
                    extension = ".ek7";
                    break;
                }
                else if (askForFileType == (3).ToString())
                {
                    extension = ".bin";
                    break;
                }
                else if (askForFileType == (4).ToString())
                {
                    extension = ".png";
                    break;
                }
                else
                {
                    // loop back to beginning if invalid entry is entered
                    Console.ForegroundColor = ConsoleColor.Red; // make text red for error
                    Console.WriteLine("\r\nInvalid entry, please try again. ");
                    Console.ResetColor();
                    continue;
                }
            }

            // Create the file name for the file
            string fileName = String.Format("213 ★ - SHUCKY - 3B979EB6757C{0}", extension);

            // make variable for user's folder directory name
            string userNameFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            
            // make variables for file path and for directory path
            string filePath = String.Empty;
            string pathString = String.Empty;

            // check whether the user is running Windows or Mac (OSX)
            if (Regex.Match(Environment.OSVersion.ToString(), @"\b(Windows)\b").Success)
            {
                pathString = $@"{userNameFolderPath}\Desktop\Shuckle";
                filePath = $@"{userNameFolderPath}\Desktop\Shuckle\{fileName}";
            }
            else if (Regex.Match(Environment.OSVersion.ToString(), @"\b(Unix)\b").Success)
            {
                pathString = $@"{userNameFolderPath}/Desktop/Shuckle";
                filePath = $@"{userNameFolderPath}/Desktop/Shuckle/{fileName}";
            }
            else
            {
                // throw exception in case of unsupported OS
                Console.ForegroundColor = ConsoleColor.Red;
                Exception OSNotSupportedException = new Exception("OS not supported");
                Console.ResetColor();
                throw OSNotSupportedException;
            }

            // check if file path exists, and if so, delete it to avoid overwrite errors
            if (File.Exists(path: filePath))
            {
                File.Delete(path: filePath);
            }

            // check whether the file directory exists
            if (Directory.Exists(path: pathString))
            {
                // use while loop to loop in case of invalid choice
                while (true)
                {
                    // ask user is they want to replace current destination folder in case of prior-existance
                    Console.ForegroundColor = ConsoleColor.Yellow; // make text yellow for warning
                    Console.Write("\r\nThere is already a folder on your desktop named `Shuckle`, would you like to replace all of its contents with the Shuckle file? [y/n] ");
                    Console.ResetColor();
                    string folderReplacement = Console.ReadLine().Trim();
                    if (folderReplacement.ToLower() == "y")
                    {
                        // delete directory if user says yes
                        Directory.Delete(path: pathString, recursive: true);
                        break;
                    }
                    else if (folderReplacement.ToLower() == "n")
                    {
                        // exit loop if user says no
                        break;
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        // loop back to beginning if user inputs invalid entry
                        Console.Write("\r\nInvalid entry, please try again.");
                        Console.ResetColor();
                        continue;
                    }
                }
            }

            // Create the subfolder.
            Directory.CreateDirectory(pathString);

            // Use Combine again to add the file name to the path string.
            string path = Path.Combine(pathString, fileName);

            // tell the user the file path
            Console.WriteLine("\r\nPath to file: {0}\r\n", path);

            // Verify the path (if id doesn't exist...)
            if (!File.Exists(path))
            {
                // use switch statement to add cases for each file type
                // then write to the data file given the path string
                // use the hex data from the `HexData.cs` file
                switch (extension) {
                    case ".pk7":
                        using (FileStream fs = File.Create(path))
                        {
                            foreach (byte b in HexData.rawDataPk7)
                            {
                                fs.WriteByte(b);
                            }
                        }
                        break;
                    case ".ek7":
                        using (FileStream fs = File.Create(path))
                        {
                            foreach (byte b in HexData.rawDataEk7)
                            {
                                fs.WriteByte(b);
                            }
                        }
                        break;
                    case ".bin":
                        using (FileStream fs = File.Create(path))
                        {
                            foreach (byte b in HexData.rawDataBin)
                            {
                                fs.WriteByte(b);
                            }
                        }
                        break;
                    case ".png":
                        using (FileStream fs = File.Create(path))
                        {
                            foreach (byte b in HexData.rawDataPng)
                            {
                                fs.WriteByte(b);
                            }
                        }
                        break;
                    default:
                        Console.ForegroundColor = ConsoleColor.Red;
                        // throw exception if invalid response was recieved
                        Exception InvalidFileExtensionException = new Exception("Invalid file extension");
                        Console.ResetColor();
                        throw InvalidFileExtensionException;
                }
            }

            Console.WriteLine("File created!\r\n");

            // use while loop to loop in case of invalid choice
            while (true)
            {
                // ask the user if they would like to view the file data
                Console.Write("Would you like to view the file data? [y/n] ");
                // get user input and convert all entries to lower case and trim any excess whitespace
                string fileData = Console.ReadLine().ToLower().Trim();
                // if they say yes then show the file data in hex, dec, oct, and bin
                if (fileData == "y")
                {
                    // Read and display the data from the file.
                    try
                    {
                        // read all bytes from hex data of generated file
                        byte[] readBuffer = File.ReadAllBytes(path);
                        Console.WriteLine("\r\nHexidecimal (base 16):");
                        Console.WriteLine("----------------------"); // 22
                        foreach (byte b in readBuffer)
                        {
                            Console.Write(b.ToString("X").ToUpper() + " ");
                        }
                        Console.WriteLine("\r\n\r\nDecimal (base 10):");
                        Console.WriteLine("------------------"); // 18
                        foreach (byte b in readBuffer)
                        {
                            Console.Write(b + " ");
                        }
                        Console.WriteLine("\r\n\r\nOctal (base 8):");
                        Console.WriteLine("---------------"); // 15
                        foreach (byte b in readBuffer)
                        {
                            Console.Write(Convert.ToString(b, 8) + " ");
                        }
                        Console.WriteLine("\r\n\r\nBinary (base 2):");
                        Console.WriteLine("----------------"); // 16
                        foreach (byte b in readBuffer)
                        {
                            Console.Write(Convert.ToString(b, 2) + " ");
                        }
                        Console.WriteLine();
                    }
                    // catch file IO exception
                    catch (IOException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    Console.Write("\r\n\r\nPress any key to exit. ");
                    Console.ReadKey();
                    Console.WriteLine();
                    break;
                }
                // exit if user denies viewing the file data
                else if (fileData == "n")
                {
                    Console.Write("\r\n\r\nPress any key to exit. ");
                    Console.ReadKey();
                    Console.WriteLine();
                    break;
                }
                // jump back to prompt (using while loop) if user inputs invalid entry
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("\r\nInvalid entry, please try again. ");
                    Console.ResetColor();
                    continue;
                }
            }
            // reset all console colors (foreground and background)
            Console.ResetColor();
        }
    }
}

